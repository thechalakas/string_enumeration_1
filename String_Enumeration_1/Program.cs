﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace String_Enumeration_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string temp = "I am not sure if pigs can fly. However, some can under the right conditions.";

            //using enumeration I can get each item 
            foreach(char c in temp)
            {
                Console.WriteLine(c);
            }

            Console.WriteLine("This is the second set of enumerations");

            //I can do this too
            for(int i=0;i<temp.Length;i++)
            {
                Console.WriteLine(temp[i]);
            }

            //this is to stop console from vanishing
            Console.ReadLine();
        }
    }
}
